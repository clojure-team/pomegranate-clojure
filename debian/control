Source: pomegranate-clojure
Section: java
Priority: optional
Maintainer: Debian Clojure Maintainers <team+clojure@tracker.debian.org>
Uploaders: Elana Hashman <ehashman@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 default-jdk,
 javahelper,
 libcognitect-test-runner-clojure,
 maven-repo-helper,
 libmaven3-core-java,
 libmaven-resolver-java,
 libmaven-resolver-transport-http-java,
 libdynapath-clojure,
 libwagon-provider-api-java,
 libwagon-http-java,
 libwagon-ssh-java,
 libhttpclient-java,
 libhttpcore-java,
 libplexus-interpolation-java,
 libplexus-component-annotations-java,
 libplexus-utils2-java,
 libtools-build-clojure
Standards-Version: 4.7.0
Homepage: https://github.com/cemerick/pomegranate
Vcs-Git: https://salsa.debian.org/clojure-team/pomegranate-clojure.git
Vcs-Browser: https://salsa.debian.org/clojure-team/pomegranate-clojure
Rules-Requires-Root: no

Package: libpomegranate-clojure
Architecture: all
Depends:
 ${java:Depends},
 ${misc:Depends},
 libmaven3-core-java,
 libmaven-resolver-java,
 libmaven-resolver-transport-http-java,
 libdynapath-clojure,
 libwagon-provider-api-java,
 libwagon-http-java,
 libwagon-ssh-java,
 libhttpclient-java,
 libhttpcore-java,
 libplexus-interpolation-java,
 libplexus-component-annotations-java,
 libplexus-utils2-java,
 libclojure-java,
Recommends:
 ${java:Recommends}
Description: dependency resolution and repository handling library for Clojure
 pomegranate provides a Clojure interface to sonatype-aether. It supports the
 following features from Aether:
 .
  *Dependency resolution and common dependency graph/hierarchy manipulation
   operations.
  *Local installation of artifacts.
  *Remote deployment.
  *Repository authentication.
  *HTTP proxy configuration.
  *Offline mode.
 .
 It also allows provides dynamic inclusion of libraries in the classpath
 whether the libraries are installed or have to be retrieved from a repository.
